package cmd

import (
	"github.com/spf13/cobra"
	"os/exec"
	"time"
)

// bootstrapCmd represents the bootstrap command
var bootstrapCmd = &cobra.Command{
	Use:   "bootstrap",
	Short: "pgo bootstrap [gitrepo] will download package and setup gometalinter",
	Long:  `pgo bootstrap [gitrepo] will download package and setup gometalinter`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) > 0 {
			StartWithStandardOutput("go", "get", "-u", args[0])
		}
		_, err := exec.LookPath("gometalinter")
		if err != nil {
			StartWithStandardOutput("go", "get", "-u", "github.com/alecthomas/gometalinter")
			time.Sleep(1*time.Second)
			StartWithStandardOutput("gometalinter", "--install")
		}
	},
}

func init() {
	RootCmd.AddCommand(bootstrapCmd)

}
